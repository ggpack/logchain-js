# Logchain

[![Pipeline status](https://gitlab.com/ggpack/logchain-js/badges/master/pipeline.svg)](https://gitlab.com/ggpack/logchain-js/pipelines)
[![Coverage report](https://gitlab.com/ggpack/logchain-js/badges/master/coverage.svg?job=test.unittest)](https://gitlab.com/ggpack/logchain-js/-/jobs)

[![Dependencies](https://img.shields.io/badge/dependencies-0-blue.svg)]()
[![License](https://img.shields.io/badge/license-ISC-blue.svg)](https://gitlab.com/ggpack/logchain-js/-/blob/master/LICENSE)
[![Package](https://img.shields.io/badge/NPM-logchain-blue.svg)](https://www.npmjs.com/package/logchain)

Logging secured by blockchain 📜⛓️

## Logs get chained
The current log line contains the signature of the previous line with your secret.
* detect lines deleted / lost
* detect logs tampering

## Philosophy
The package is intended to be a **lightweight** util for generating **incorruptible** logs.

For this pupose we rely as much as possible on standard packages: few dependencies, high quality.

The formatters are easily **extensible** by simply deriving from `Basic`.


# Usage

## Install
``` bash
npm install logchain
```

## Choose your log type
Many types of logs are supported out-of-the-box:
- `Basic` raw text, relying on the standard formatter
- `Json` structured log lines with static & dynamic fields

You can write a custom formatter in 20-ish lines.

## Init once in main
``` javascript
import { LogChainer } from "logchain"

// Initialize a default chainer.
const chainer = new LogChainer()

// Register the formatter to the logger.
chainer.initLogging()
```

Have a look at [the comprehensive guide of constructor parameters](#constructor-parameters).


## Use everywhere with standard logging
Standard console methods are overriden once per program.
``` javascript
console.log("the basic log")
console.debug("the debug")
console.info("the info")
console.warn("the warning")
console.error("the error")
```

## Check your logs integrity afterwards
``` javascript
import { LogChainer } from "logchain"

const aLogChain = [
	"2020-06-03T22:00:17.566Z LOG basicChaining.test.js:28 the basic log |43d85615af018369",
	"2020-06-03T22:00:17.567Z DEBU basicChaining.test.js:29 the debug |2cdcac6830d7d094",
	"2020-06-03T22:00:17.568Z INFO basicChaining.test.js:30 the info |72f847d26583d23d",
	"2020-06-03T22:00:17.568Z WARN basicChaining.test.js:31 the warning |095e94f80e612f95",
	"2020-06-03T22:00:17.569Z ERRO basicChaining.test.js:32 the error |618c0f4dd07a3942"
]

const chainer = new LogChainer({secret: "9b06b72c717e8d9f8c1393e11f2635c5"})
const result = chainer.verify(aLogChain)

if(result.value)
{
	console.log("All right")
}
else
{
	console.log("Last good line", result.prevLine)
	console.log("First bad line", result.line)
}
```

## Constructor parameters

They are passed as an object.
``` javascript
import { LogChainer } from "logchain"

const chainer = new LogChainer({verbosity: 3, secret: "mySignatureKey"})
```

| **Param** *Type* | Default value | Description |
| ----- | ------------- | ----------- |
| **formatterCls** *class* | formatters.Basic | Type of logging to perform, raw text, json, custom |
| **template** *string* | see below | Placeholder string used by raw-text loggers |
| **secret** *string* | random of 128 chars | Signature key to compute the line signature |
| **seed** *string* | random of 64 chars | Random string to sign into the first log line |
| **stream** *stream* | stdout | Where the logs are sent, file/console/custom stream |
| **verbosity** *int* | 0 | Number [0..5] mapped to a logging.level |


## Logchain record logging fields
We make the record with some handy fields:

| Name | Description |
| ---- | ----------- |
| **fileLine** | Widespread `filename:lineno` |
| **levelLetter** | First letter of the logging level names |
| **msg** | The text being logged |
| **process** | The PID as int |
| **processName** | The process name |
| **signature** | The digital signature of the previous line. Include it in all your lines to benefit from the chaining |
| **timestamp** | ISO8601 precise to the milliseconds |

----

# Contributing

## Install
[**The code is hosted on Gitlab 🦊**](https://gitlab.com/ggpack/logchain-js)

Simply clone and submit pull requests.

## Testing
The unit tests are located in `test` and discovered by `jest`.

``` bash
# Run all
npm test
```

## Releasing
The process is triggered by a tag added to a commit. The tag must match the pattern `release_<VERSION>`
and `VERSION` has to comply to **[semver](https://semver.org)**.

[A CI/CD job](https://gitlab.com/ggpack/logchain-js/-/blob/master/.gitlab-ci.yml) handles the new tag event and publishes the package to npmjs.

# Thanks
[Freepik](https://www.flaticon.com/authors/freepik) from [www.flaticon.com](https://www.flaticon.com) for the icon.

[Shu](https://github.com/chibicode) for the npm module name.
