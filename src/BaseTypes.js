
export const levels = {
	DEBUG:   10,
	INFO:    20,
	WARNING: 30,
	ERROR:   40,
}

export const verbosityToLevel = {
	0: levels.ERROR,
	1: levels.WARNING,
	2: levels.INFO,
	3: levels.DEBUG,
}
