import * as formatters from "./formatters/index.js"
import {levels, verbosityToLevel} from "./BaseTypes.js"

export {formatters}

export class Logger
{
	constructor(iLevel, iFormatter)
	{
		this.level = iLevel
		this.handlers = []
		this.formatter = iFormatter
	}

	addHandler(iHandler)
	{
		if(!this.handlers.includes(iHandler))
		{
			this.handlers.push(iHandler)
		}
	}

	logBase(level, message)
	{
		if(level >= this.level)
		{
			const record = this.formatter.makeRecord(level, message)
			const formattedOut = this.formatter.format(record)
			this.handlers.forEach(h => h.write(formattedOut + "\n"))
		}
	}
}

export class LogChainer
{
	constructor(args = {})
	{
		this.stream = args.stream || process.stdout
		this.formatterCls = args.formatterCls || formatters.Basic
		this.formatter = new this.formatterCls(args)
		this.verbosity = args.verbosity || 0
	}

	initLogging()
	{
		const aLevel = verbosityToLevel[this.verbosity] || levels.DEBUG
		const logger = new Logger(aLevel, this.formatter)
		logger.addHandler(this.stream)

		// Override
		//console.log   = logger.logBase.bind(logger, "LOG") // Keep it for dev work
		console.debug = logger.logBase.bind(logger, levels.DEBUG)
		console.info  = logger.logBase.bind(logger, levels.INFO)
		console.warn  = logger.logBase.bind(logger, levels.WARNING)
		console.error = logger.logBase.bind(logger, levels.ERROR)

		return logger
	}

	verify(iLogChain)
	{
		for(let idx = 0; idx < iLogChain.length - 1; idx++)
		{
			const prevLine = iLogChain[idx]
			const line = iLogChain[idx + 1]

			const isValid = this.formatter.verify(prevLine, line)

			if(!isValid)
			{
				return {
					line,
					prevLine,
					value: false
				}
			}
		}
		return {value: true}
	}

	/*
		Adds contextual data to the log record via a fields object {key: value}
		Remove a key by setting it to `null`.
	*/
	setFields(fields)
	{
		this.formatter.setFields(fields)
	}

	withManagedFields(fields, callback)
	{
		let previousCtx = this.formatter.setFields(fields)
		try
		{
			callback()
		}
		finally
		{
			this.formatter.context = previousCtx
		}
	}
}
