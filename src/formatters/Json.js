import { Basic } from "./Basic.js"

export class Json extends Basic
{
	constructor(args)
	{
		super(args)
		const defaultFields = ["signature", "timestamp", "fileLine", "levelLetter", "msg", "process", "processName"]

		// Merge fields & extraFields
		this.fields = [...new Set((args.fields || defaultFields).concat(args.extraFields || []))]
	}

	stringify(rec)
	{
		const subRecord = this.fields.reduce((accum, curr) =>
		{
			accum[curr] = rec[curr]
			return accum
		}, {})

		Object.assign(subRecord, this.context)
		return JSON.stringify(subRecord)
	}

	extractSignature(line)
	{
		return JSON.parse(line).signature
	}
}
