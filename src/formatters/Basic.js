import crypto from "crypto"

import {levels} from "../BaseTypes.js"


function interpolate(format, dict)
{
	const names = Object.keys(dict)
	const vals = Object.values(dict)
	return new Function(...names, `return \`${format}\`;`)(...vals)
}

const levelToName = Object.entries(levels).reduce((accu, curr) => {accu[curr[1]] = curr[0]; return accu}, {})

export class Basic
{
	constructor(args)
	{
		this.secret = args.secret || crypto.randomBytes(128).toString("hex")
		this.prevLine = args.seed || crypto.randomBytes(64).toString("hex")

		this.template = args.template || "${timestamp} ${levelLetter} ${fileLine} ${msg} |${signature}"
		this.context = {}
	}
	
	makeRecord(level, msg)
	{
		const err = new Error()
		// FileLine is mostly used as-is. No need to split them and re-join.
		const regex = /.*\/(.*?:\d+)/

		//process.stdout.write(err.stack.split("\n")[3] + "\n")
		// The stack deps depends on the implem.
		const match = regex.exec(err.stack.split("\n")[3])
		return {
			msg,
			level,
			fileLine: match[1],
			levelLetter: levelToName[level][0],
			process: process.pid,
			processName: process.title,
			signature: this.sign(this.prevLine, this.secret),
			timestamp: new Date().toISOString()
		}
	}

	format(record)
	{
		this.prevLine = this.stringify(record)
		return this.prevLine
	}

	stringify(rec)
	{
		return interpolate(this.template, rec)
	}

	extractSignature(line)
	{
		return line.slice(line.lastIndexOf("|") + 1)
	}

	/*
		Generates a truncated signature of the input message.
		@param length: controls the size of the signature, set it to None for full length.
	*/
	sign(message, secret, length = 16)
	{
		return crypto.createHmac("sha256", secret)
		.update(message)
		.digest("hex")
		.substring(0, length)
	}

	// Checks the line's signature matches prevLine's content
	verify(prevLine, line)
	{
		const aStoredSign = this.extractSignature(line)
		const aComputedSign = this.sign(prevLine, this.secret)
		return crypto.timingSafeEqual(Buffer.from(aStoredSign), Buffer.from(aComputedSign))
	}

	setFields(fields)
	{
		const previousCtx = {...this.context}
		Object.assign(this.context, fields)
	
		// Purge null values
		Object.keys(this.context).forEach(k => this.context[k] == null && delete this.context[k])
		return previousCtx
	}

}
