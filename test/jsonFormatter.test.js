import { LogChainer, formatters } from "../src/index.js"
import { MockStream } from "./MockStream.js"
import crypto from "crypto"


function includesKeys(iObj, iKeys)
{
	return iKeys.every(k => k in iObj)
}

test("ctor", () =>
{
	let chainer = new LogChainer({formatterCls: formatters.Json})

	let fields = chainer.formatter.fields
	expect(fields).toContain("msg")
	expect(fields).toContain("timestamp")
	expect(fields).toContain("signature")
	expect(fields).not.toContain("custom")

	// Extra fields
	chainer = new LogChainer({
		formatterCls: formatters.Json,
		extraFields: ["custom"]	
	})
	fields = chainer.formatter.fields
	expect(fields).toContain("msg")
	expect(fields).toContain("custom")

	// Fine tuning fields
	chainer = new LogChainer({
		formatterCls: formatters.Json,
		fields: ["a", "b"],
		extraFields: ["c"]
	})
	fields = chainer.formatter.fields
	expect(fields).toEqual(["a", "b", "c"])	
})

test("content", () =>
{
	// Grab the logging output
	const logStream = new MockStream()
	const chainer = new LogChainer({formatterCls: formatters.Json,
		stream: logStream,
		verbosity: 5,
		secret: "Et2FwQefvb7HfCb7tATguSicVj_7TVlM"})
	chainer.initLogging()

	// Some varied content
	console.debug("Hello debug இ éïô $*µ$,;:!§/.?<>|ç&[ 3² + 4² = 5² ]")
	console.info("this is useful info")
	console.warn("warning!")
	console.error("error message")

	const lines = logStream.store
	expect(lines).toHaveLength(4)

	//console.log(lines)

	const firstLine = JSON.parse(lines[0])
	const keys = ["signature", "timestamp", "fileLine", "levelLetter", "msg", "process"]

	expect(includesKeys(firstLine, keys)).toEqual(true)
	expect(firstLine.msg).toEqual("Hello debug இ éïô $*µ$,;:!§/.?<>|ç&[ 3² + 4² = 5² ]")
})

test("verify", () =>
{
	const chainer = new LogChainer({formatterCls: formatters.Json, secret: "Et2FwQefvb7HfCb7tATguSicVj_7TVlM"})
	
	/* eslint-disable quotes */
	let lines = [
		'{"fileLine": "TestJsonFormatter.py:48", "levelno": 10, "msg": "Hello debug இ éïô $*µ$,;:!§/.?<>|ç&[ 3² + 4² = 5² ]", "process": 13615, "processName": "MainProcess", "signature": "67c2369cb5386541", "thread": 140242165983040, "threadName": "MainThread", "timestamp": "2020-04-26 13:56:32.048"}',
		'{"fileLine": "TestJsonFormatter.py:49", "levelno": 20, "msg": "this is useful info", "process": 13615, "processName": "MainProcess", "signature": "d7b8f8da61f214ee", "thread": 140242165983040, "threadName": "MainThread", "timestamp": "2020-04-26 13:56:32.048"}',
		'{"fileLine": "TestJsonFormatter.py:50", "levelno": 30, "msg": "warning!", "process": 13615, "processName": "MainProcess", "signature": "795ddd316dfcf908", "thread": 140242165983040, "threadName": "MainThread", "timestamp": "2020-04-26 13:56:32.048"}',
		'{"fileLine": "TestJsonFormatter.py:51", "levelno": 40, "msg": "error message", "process": 13615, "processName": "MainProcess", "signature": "93ade9e6e134639b", "thread": 140242165983040, "threadName": "MainThread", "timestamp": "2020-04-26 13:56:32.049"}',
		'{"fileLine": "TestJsonFormatter.py:52", "levelno": 50, "msg": "Good bye.", "process": 13615, "processName": "MainProcess", "signature": "ade925c004dca5dd", "thread": 140242165983040, "threadName": "MainThread", "timestamp": "2020-04-26 13:56:32.049"}'
	]

	let result = chainer.verify(lines)
	expect(result.value).toEqual(true)

	// Deletion
	lines = [
		'{"fileLine": "TestJsonFormatter.py:49", "levelno": 20, "msg": "this is useful info", "process": 13615, "processName": "MainProcess", "signature": "d7b8f8da61f214ee", "thread": 140242165983040, "threadName": "MainThread", "timestamp": "2020-04-26 13:56:32.048"}',
		'{"fileLine": "TestJsonFormatter.py:52", "levelno": 50, "msg": "Good bye.", "process": 13615, "processName": "MainProcess", "signature": "ade925c004dca5dd", "thread": 140242165983040, "threadName": "MainThread", "timestamp": "2020-04-26 13:56:32.049"}'
	]
	result = chainer.verify(lines)
	expect(result.value).toEqual(false)

	// Tampering
	lines = [
		'{"fileLine": "FakeFile.py:49", "levelno": 20, "msg": "this is useful info", "process": 13615, "processName": "MainProcess", "signature": "d7b8f8da61f214ee", "thread": 140242165983040, "threadName": "MainThread", "timestamp": "2020-04-26 13:56:32.048"}',
		'{"fileLine": "TestJsonFormatter.py:50", "levelno": 30, "msg": "warning!", "process": 13615, "processName": "MainProcess", "signature": "795ddd316dfcf908", "thread": 140242165983040, "threadName": "MainThread", "timestamp": "2020-04-26 13:56:32.048"}',
	]
	/* eslint-enable quotes */
	result = chainer.verify(lines)
	expect(result.value).toEqual(false)
})

test("update_context", () =>
{
	
	// Concrete example: an App handling transactions
	// Set some contextual information
	class App
	{
		constructor(appName, logger)
		{
			this.logger = logger
			this.logger.setFields({appName})
			console.info("Creating the app")
		}

		handleTransaction(userId, callback)
		{
			const trxId = crypto.randomBytes(64).toString("hex")
			this.logger.withManagedFields({userId, trxId}, callback)
		}

		close()
		{
			console.info("Closing the app")
		}
	}

	// The log chain in transparent for the callbacks
	function callback1()
	{
		console.warn("Something happened")
	}

	function callback2()
	{
		console.info("Serving a resource")
	}


	const logStream = new MockStream()
	const params = {
		stream: logStream,
		verbosity: 5,
		formatterCls: formatters.Json,
		fields: ["signature", "msg"]
	}

	const chainer = new LogChainer(params)
	chainer.initLogging()

	const app = new App("MyApp", chainer)
	app.handleTransaction("user1", callback1)
	app.handleTransaction("user1", callback2)
	app.close()

	const lines = logStream.store
	expect(lines).toHaveLength(4)

	const result = chainer.verify(lines)
	expect(result.value).toEqual(true)

	const line0 = JSON.parse(lines[0])
	expect(includesKeys(line0, ["appName", "msg"])).toEqual(true)

	const line1 = JSON.parse(lines[1])
	expect(includesKeys(line1, ["appName", "msg", "trxId", "userId"])).toEqual(true)

	const line2 = JSON.parse(lines[2])
	expect(includesKeys(line2, ["appName", "msg", "trxId", "userId"])).toEqual(true)
	expect(line1.trxId).not.toEqual(line2.trxId)

	const line3 = JSON.parse(lines[3])
	expect(includesKeys(line3, ["trxId", "userId"])).toEqual(false)
	expect(includesKeys(line3, ["appName", "msg"])).toEqual(true)
})
