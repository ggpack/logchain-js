export class MockStream
{
	constructor()
	{
		this.store = []
	}
	write(line)
	{
		this.store.push(line.trim())
	}
}
