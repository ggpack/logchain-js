import { LogChainer } from "../src/index.js"
import {levels} from "../src/BaseTypes.js"
import { MockStream } from "./MockStream.js"

test("ctor", () =>
{
	// Default args
	const chainer1 = new LogChainer()
	
	expect(chainer1.formatter).toBeDefined()
	const formatter = chainer1.formatter

	expect(formatter.secret).toBeDefined()
	expect(formatter.secret.length).toBeGreaterThan(100)
	expect(formatter.prevLine).toBeDefined()
	expect(formatter.prevLine.length).toBeGreaterThan(50)

	expect(chainer1.verbosity).toEqual(0)
	expect(chainer1.stream).toBe(process.stdout)

	// Specific args
	const chainer2 = new LogChainer({verbosity: 3})
	expect(chainer2.verbosity).toEqual(3)
})

test("logging_happy_case", () =>
{
	const logStream = new MockStream()
	const chainer = new LogChainer({stream: logStream})

	const logger = chainer.initLogging()

	console.debug("the debug")
	console.info("the info")
	console.warn("the warning")
	console.error("the error1")

	// First stage: the default level is ERROR
	//process.stdout.write(logStream.store)
	let lines = logStream.store
	expect(lines).toHaveLength(1)

	let result = chainer.verify(lines)
	expect(result.value).toEqual(true)

	logStream.store = []
	logger.level = levels.DEBUG
	console.debug("the debug")
	console.info("the info")
	console.warn("the warning")
	console.error("the error2")

	// Second stage: the level is DEBUG
	lines = logStream.store
	expect(lines).toHaveLength(4)

	result = chainer.verify(lines)
	expect(result.value).toEqual(true)
})

test("verify_happy_case", () =>
{
	const aLogChain = [
		"2020-06-03T22:00:17.566Z LOG basicChaining.test.js:28 the basic log |43d85615af018369",
		"2020-06-03T22:00:17.567Z DEBU basicChaining.test.js:29 the debug |2cdcac6830d7d094",
		"2020-06-03T22:00:17.568Z INFO basicChaining.test.js:30 the info |72f847d26583d23d",
		"2020-06-03T22:00:17.568Z WARN basicChaining.test.js:31 the warning |095e94f80e612f95",
		"2020-06-03T22:00:17.569Z ERRO basicChaining.test.js:32 the error |618c0f4dd07a3942"
	]

	const chainer = new LogChainer({secret: "9b06b72c717e8d9f8c1393e11f2635c5"})
	const result = chainer.verify(aLogChain)
	expect(result.value).toEqual(true)
})

test("checking_missing_line", () =>
{
	const aLogChain = [
		"2020-06-03T22:00:17.566Z LOG basicChaining.test.js:28 the basic log |43d85615af018369",
		"2020-06-03T22:00:17.568Z INFO basicChaining.test.js:30 the info |72f847d26583d23d",
		"2020-06-03T22:00:17.568Z WARN basicChaining.test.js:31 the warning |095e94f80e612f95",
		"2020-06-03T22:00:17.569Z ERRO basicChaining.test.js:32 the error |618c0f4dd07a3942"
	]

	const chainer = new LogChainer({secret: "9b06b72c717e8d9f8c1393e11f2635c5"})
	const result = chainer.verify(aLogChain)
	expect(result.value).toEqual(false)
	expect(result.prevLine).toEqual(aLogChain[0])
	expect(result.line).toEqual(aLogChain[1])
})

test("checking_wrong_signature", () =>
{
	const aLogChain = [
		"2020-06-03T22:00:17.566Z LOG basicChaining.test.js:28 the basic log |43d85615af018369",
		"2020-06-03T22:00:17.567Z DEBU basicChaining.test.js:29 the debug |2cdcac6830d7d094",
		"2020-06-03T22:00:17.568Z INFO basicChaining.test.js:30 the hack  |72f847d26583d23d",
		"2020-06-03T22:00:17.568Z WARN basicChaining.test.js:31 the warning |095e94f80e612f95",
		"2020-06-03T22:00:17.569Z ERRO basicChaining.test.js:32 the error |618c0f4dd07a3942"
	]

	const chainer = new LogChainer({secret: "9b06b72c717e8d9f8c1393e11f2635c5"})
	const result = chainer.verify(aLogChain)
	expect(result.value).toEqual(false)
	expect(result.prevLine).toEqual(aLogChain[2])
	expect(result.line).toEqual(aLogChain[3])
})
